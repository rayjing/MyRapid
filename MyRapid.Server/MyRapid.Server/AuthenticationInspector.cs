/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * 初版作者: 陈恩点
 * 创建时间: 2012/8/21 11:49:53
 * 联系方式: 18115503914
 * 简单描述: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using MyRapid.Code;
using System.Configuration;
using System.Data.Common;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Text;
using System.Xml;
using MyRapid.Server;

namespace MyRapid.Server.Extension
{

    public class AuthenticationInspector : IDispatchMessageInspector
    {
        //string License = ConfigurationManager.AppSettings["License"];
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            //这里可以做访问控制
            //例如证书控制，或者只允许某些IP访问

            //if (request.Headers.FindHeader("License", "Token") >= 0)
            //{
            //    string clientLicense = request.Headers.GetHeader<string>("License", "Token");
            //    if (clientLicense != License)
            //        throw new Exception("Authentication: 客户端证书与服务器不匹配 !");
            //}
            //else
            //{
            //    throw new Exception("Authentication: 请提供证书 !");
            //}

            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //要对返回数据处理需要先复制一份
            //reply只能被读取一次
            //MessageBuffer requstBuffer = reply.CreateBufferedCopy(int.MaxValue);
            //Message msg = requstBuffer.CreateMessage();
            //reply = requstBuffer.CreateMessage();
        }


    }




}